class Book{
    id: string;
    title: string;
    author: Author;
    year: number;
    genre: string[]
    constructor(id: string, title: string, author: Author, year: number, genre: string[]){

        if(!id || !title || !author || !year){
            throw Error(`Invalid parameters id: ${id} name: ${title} author: ${author} year: ${year}`);
        }

        this.id = id;
        this.title = title;
        this.author = author;
        this.year = year;
        this.genre = genre ?? [];
    }
}

class Author{
    name: string;

    constructor(name: string){
        if(!name){
            throw Error(`Invalid name for author: ${name}`);
        }

        this.name = name;
    }
}

export default Book;