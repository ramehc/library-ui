import { render } from "@testing-library/react"
import { PropsWithChildren } from "react";
import { BrowserRouter, Route } from "react-router-dom"

  
export const renderRedirect = (redirectUrl: string, children: PropsWithChildren<any>) => {

  return render(<BrowserRouter>
    {children}
    <Route path={redirectUrl} render={() => <>{redirectUrl}</>} />
  </BrowserRouter>

  );
}
