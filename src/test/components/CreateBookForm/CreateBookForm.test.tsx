import { FetchResult } from "@apollo/client";
import { MockedProvider } from "@apollo/client/testing";
import { MockedResponse, ResultFunction } from "@apollo/client/utilities/testing/mocking/mockLink";
import { fireEvent, render, screen } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router, Route } from "react-router-dom";
import CreateBookForm from "../../../components/CreateBookForm/CreateBookForm";
import { ADD_BOOK } from "../../../graphql-queries";
import { renderRedirect } from "../helpers/RedirectTestHelper";
import '@testing-library/jest-dom';
import { act } from "react-dom/test-utils";

const libraryId = 'libraryId';
const title = 'Title';
const year = 1995;
const author = 'Test';
const errorMessage = 'An error occurred';

const addBookMutation: ResultFunction<FetchResult<{
    [key: string]: any;
}, Record<string, any>, Record<string, any>>> = jest.fn(() => ({
    data: {
        createBook: true
    }
}));

const getAddBookMocks = (returnResult = false) => {
    return [
        {
            request: {
                query: ADD_BOOK,
                variables: {
                    libraryId: libraryId,
                    title: title,
                    year: year,
                    author: author,
                    genre: []
                },
            },
            newData: returnResult ? addBookMutation : undefined,
            error: returnResult ? undefined : new Error(errorMessage)
        }
    ];
}


const renderFormWithLibraryId = (mocks: readonly MockedResponse<Record<string, any>>[] | undefined) => {
    const history = createMemoryHistory({ initialEntries: [`/library/createbook/${libraryId}`] });
    return render(<Router history={history}>
        <Route path='/library/createbook/:libraryId' render={() => <MockedProvider mocks={mocks} addTypename={false}>
            <CreateBookForm />
        </MockedProvider>} />
    </Router>

    );
}


describe('Create Book Form', () => {
    it('Redirects to /library if libraryId is undefined', () => {
        const redirectUrl = '/library/';
        const mocks = getAddBookMocks(true);
        const renderResult = renderRedirect(redirectUrl, <MockedProvider mocks={mocks} addTypename={false}><CreateBookForm /></MockedProvider>);
        expect(renderResult.container.innerHTML).toEqual(expect.stringContaining(redirectUrl));
    });

    it('Title is a required field', async () => {
        const mocks = getAddBookMocks(true);
        renderFormWithLibraryId(mocks);

        const titleInput = await screen.findByTitle('title');
        expect(titleInput).toBeRequired();
    });

    it('Author is a required field', async () => {
        const mocks = getAddBookMocks(true);
        renderFormWithLibraryId(mocks);

        const authorInput = await screen.findByTitle('author');
        expect(authorInput).toBeRequired();
    });

    it('Year is a required field', async () => {
        const mocks = getAddBookMocks(true);
        renderFormWithLibraryId(mocks);

        const yearInput = await screen.findByTitle('year');
        expect(yearInput).toBeRequired();
    });

    it('Add book is called when form is submitted', async () => {
        const mocks = getAddBookMocks(true);
        renderFormWithLibraryId(mocks);

        await fillForm();

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        expect(addBookMutation).toBeCalledTimes(1);
    });

    it('Error message is displayed if add book call fails', async () => {
        const mocks = getAddBookMocks();
        renderFormWithLibraryId(mocks);

        await fillForm();

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        const errorTextElement = screen.getByText(errorMessage);

        expect(errorTextElement).toBeInTheDocument();
    });

    it('Redirects to library books display when book is created', async () => {
        const redirectUrl = `/library/books/${libraryId}`;
        const mocks = getAddBookMocks(true);
        const history = createMemoryHistory({ initialEntries: [`/library/createbook/${libraryId}`] });

        const renderResult = render(<Router history={history}>
            <Route path='/library/createbook/:libraryId' render={() => <MockedProvider mocks={mocks} addTypename={false}>
                <CreateBookForm />
            </MockedProvider>} />
            <Route path={redirectUrl} render={() => <div>{redirectUrl}</div>} />
        </Router>);

        await fillForm();

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        expect(renderResult.container.innerHTML).toEqual(expect.stringContaining(redirectUrl));
    });
});

async function fillForm() {
    const submitButton = await screen.findByText('Create');
    const yearInput = await screen.findByTitle('year');
    const authorInput = await screen.findByTitle('author');
    const titleInput = await screen.findByTitle('title');

    fireEvent.change(titleInput, { target: { value: title } });
    fireEvent.change(authorInput, { target: { value: author } });
    fireEvent.change(yearInput, { target: { value: year } });
    fireEvent.click(submitButton);
}
