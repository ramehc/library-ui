import BookDisplay from "../../../components/BookDisplay/BookDisplay";
import { MockedProvider, MockedResponse } from '@apollo/client/testing';
import { DELETE_BOOK, LIBRARY_BOOKS_QUERY } from '../../../graphql-queries';
import Book from '../../../Interfaces/book';
import { act, fireEvent, render, RenderResult, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import { Route, Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { ResultFunction } from "@apollo/client/utilities/testing/mocking/mockLink";
import { FetchResult } from "@apollo/client";
import { renderRedirect } from "../helpers/RedirectTestHelper";

const libraryId = 'libraryId';
const libraryBranch = 'Any Branch';
const errorMessage = 'An error occurred';
const title = 'Any Title';
const genre = 'Any Genre';
const year = 1995;
const author = 'Any Author';
const deleteBookMutation: ResultFunction<FetchResult<{
  [key: string]: any;
}, Record<string, any>, Record<string, any>>> = jest.fn(() => ({
  data: {
    deleteBook: true
  }
}));

const libraryBooksQuery : ResultFunction<FetchResult<{
  [key: string]: any;
}, Record<string, any>, Record<string, any>>> =  jest.fn(() => ({
  data: {
    getLibraryBooks: defaultBooks,
    getLibrary: { branch: libraryBranch }
  }
}))



const defaultBooks: Array<Book> = [
  {
    id: 'Any Valid Id',
    year: year,
    genre: [
      genre
    ],
    title: title,
    author: {
      name: author
    }
  },
  {
    id: 'Any Valid Id 2',
    year: year + 1,
    genre: [
      genre + ' 1'
    ],
    title: title,
    author: {
      name: author + ' 1'
    }
  },
  {
    id: 'Any Valid Id 3',
    year: year + 2,
    genre: [
      genre + ' 2'
    ],
    title: title,
    author: {
      name: author + ' 2'
    }
  },
];

const getLibraryBooksMocks = (returnResult = true) => {


  return  [
    {
      request: {
        query: LIBRARY_BOOKS_QUERY,
        variables: {
          libraryId: libraryId,
        },
      },
      newData: returnResult ? libraryBooksQuery : undefined,
      error: returnResult ? undefined : new Error(errorMessage)
    },
    {
      request: {
        query: DELETE_BOOK,
        variables: {
          libraryId: libraryId,
          bookId: defaultBooks[0].id
        },
      },
      newData: deleteBookMutation
    }
  ];
}

const renderWithLibraryId = (mocks: readonly MockedResponse<Record<string, any>>[] | undefined) => {
  const history = createMemoryHistory({ initialEntries: [`/library/${libraryId}`] });
  return render(<Router history={history}> {
    <Route path='/library/:libraryId' render={() => <>
      <MockedProvider mocks={mocks} addTypename={false}>
        <BookDisplay />
      </MockedProvider>
    </>} />
  }</Router>
  );
};

describe('Book Display', () => {

  it('Redirects to /library/ if no library ID is in url params', () => {
    const mocks = getLibraryBooksMocks();
    const redirectUrl = '/library/';
    const renderResult = renderRedirect(redirectUrl, <><MockedProvider mocks={mocks} addTypename={false}>
      <BookDisplay />
    </MockedProvider> </> );

    expect(renderResult.container.innerHTML.trim()).toEqual(redirectUrl);

  });

  it('Loading screen is shown when page data is loading', () => {
    const mocks = getLibraryBooksMocks();

    const bookDisplay = renderWithLibraryId(mocks);

    expect(bookDisplay.container.firstElementChild?.classList.contains('loading__container')).toBe(true);

  });


  it('Error screen is shown when there is an error loading page data', async () => {
    const mocks = getLibraryBooksMocks(false);

    renderWithLibraryId(mocks);

    await act(async () => {
      await new Promise(resolve => setTimeout(resolve, 0));
    });

    const errorTextElement = screen.getByText(errorMessage);

    expect(errorTextElement).toBeInTheDocument();
  });

  describe('Displaying Books', () => {

    const mocks = getLibraryBooksMocks();

    beforeEach(async () => {
      renderWithLibraryId(mocks);

      await act(async () => {
        await new Promise(resolve => setTimeout(resolve, 0));
      });
    })

    it('The number of books that are displayed matches number of book elements in response', async () => {

      const titleElements = await screen.findAllByText(title);

      expect(titleElements).toHaveLength(defaultBooks.length);
    });

    it('Library title is displayed', async () => {

      const libraryTitleElement = await screen.findByText(`${libraryBranch} Library`);

      expect(libraryTitleElement).toBeInTheDocument();
    });

    it('Book author is displayed', async () => {

      const authorElement = await screen.findByText(author);

      expect(authorElement).toBeInTheDocument();
    });

    it('Book year is displayed', async () => {

      const yearElement = await screen.findByText(year);

      expect(yearElement).toBeInTheDocument();
    });

    it('Book genre is displayed', async () => {

      const genreElement = await screen.findByText(genre);

      expect(genreElement).toBeInTheDocument();
    });
  });

  describe('Delete Modal', () => {
    const mocks = getLibraryBooksMocks();
    let renderResult: RenderResult;

    beforeEach(async () => {
      jest.clearAllMocks();
      renderWithLibraryId(mocks);

      await act(async () => {
        await new Promise(resolve => setTimeout(resolve, 0));
      });

      const deleteIcons = screen.getAllByTitle('delete__icon');

      if(deleteIcons.length === 0){
        fail('Cannot find delete icon');
      }

      const deleteIcon =  deleteIcons[0];

      act(() => {
        fireEvent.click(deleteIcon);
      });

    })

    it('Modal is displayed when delete icon is clicked', async () => {
      const deleteModal = screen.getByTitle('delete__modal');
      
      expect(deleteModal).toBeVisible();
    });

    it('Cancel button is displayed when delete icon is clicked', async () => {
      const cancelButton = await screen.findByText('Cancel');

      expect(cancelButton).toBeVisible();
    });

    it('Delete button is displayed when delete icon is clicked', async () => {

      const deleteButton = await screen.findByText('Delete');

      expect(deleteButton).toBeVisible(); 
    });

    it('Modal is dismissed when cancel button is clicked', async () => {
      const cancelButton = await screen.findByText('Cancel');
      const deleteModal =  await screen.findByTitle('delete__modal');

      act(() => {
        fireEvent.click(cancelButton);
      });

      expect(deleteModal).not.toBeInTheDocument();
    });

    it('When cancel button is clicked, delete mutation is not called', async () => {
      const cancelButton = await screen.findByText('Cancel');

      act(() => {
        fireEvent.click(cancelButton);
      });

      expect(deleteBookMutation).not.toHaveBeenCalled();
    });

    it('When delete button is clicked delete mutation is called', async () => {
      const deleteButton = await screen.findByText('Delete');

      await act(async () => {
        fireEvent.click(deleteButton);
        await new Promise(resolve => setTimeout(resolve, 0)); //For graphql mock provider to update
      });

      expect(deleteBookMutation).toHaveBeenCalledTimes(1);

    });

    it('When delete mutation is successful, get books is queried again', async () => {
      const deleteButton = await screen.findByText('Delete');

      await act(async () => {
        fireEvent.click(deleteButton);
        await new Promise(resolve => setTimeout(resolve, 0)); //For graphql mock provider to update
      });

      expect(libraryBooksQuery).toHaveBeenCalledTimes(2);

    });


  });

  
});