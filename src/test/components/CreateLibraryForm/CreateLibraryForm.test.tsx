import { FetchResult } from "@apollo/client";
import { MockedProvider } from "@apollo/client/testing";
import { MockedResponse, ResultFunction } from "@apollo/client/utilities/testing/mocking/mockLink";
import { fireEvent, render, screen } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router, Route, BrowserRouter } from "react-router-dom";
import { ADD_LIBRARY } from "../../../graphql-queries";
import '@testing-library/jest-dom';
import { act } from "react-dom/test-utils";
import CreateLibraryForm from "../../../components/CreateLibraryForm/CreateLibraryForm";

const libraryId = 'libraryId';
const name = 'Name';
const errorMessage = 'An error occurred';

const addLibraryMutation: ResultFunction<FetchResult<{
    [key: string]: any;
}, Record<string, any>, Record<string, any>>> = jest.fn(() => ({
    data: {
        createLibrary: true
    }
}));

const getAddLibraryMocks = (returnResult = true) => {
    return [
        {
            request: {
                query: ADD_LIBRARY,
                variables: {
                    name: name,
                },
            },
            newData: returnResult ? addLibraryMutation : undefined,
            error: returnResult ? undefined : new Error(errorMessage)
        } 
    ];
}


const renderForm = (mocks: readonly MockedResponse<Record<string, any>>[] | undefined) => {
    return render(
        <BrowserRouter> <MockedProvider mocks={mocks} addTypename={false}>
            <CreateLibraryForm />
        </MockedProvider>
        </BrowserRouter>
        
    );
}


describe('Create Library Form', () => {
    it('Name is a required field', async () => {
        const mocks = getAddLibraryMocks();
        renderForm(mocks);

        const nameInput = await screen.findByTitle('name');
        expect(nameInput).toBeRequired();
    });

  

    it('Add library is called when form is submitted', async () => {
        const mocks = getAddLibraryMocks();
        renderForm(mocks);

        await fillForm();

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        expect(addLibraryMutation).toBeCalledTimes(1);
    });

    it('Error message is displayed if add library call fails', async () => {
        const mocks = getAddLibraryMocks(false);
        renderForm(mocks);

        await fillForm();

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        const errorTextElement = screen.getByText(errorMessage);

        expect(errorTextElement).toBeInTheDocument();
    });

    it('Redirects to library display when library is created', async () => {
        const redirectUrl = `/library/`;
        const mocks = getAddLibraryMocks();
        const history = createMemoryHistory({ initialEntries: [`/library/create/`] });

        const renderResult = render(<Router history={history}>
            <Route path='/library/create/' render={() => <MockedProvider mocks={mocks} addTypename={false}>
                <CreateLibraryForm />
            </MockedProvider>} />
            <Route path={redirectUrl} render={() => <>{redirectUrl}</>} />
        </Router>);

        await fillForm();

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        expect(renderResult.container.innerHTML.trim()).toEqual(redirectUrl);
    });
});

async function fillForm() {
    const submitButton = await screen.findByText('Create');
    const nameInput = await screen.findByTitle('name');

    fireEvent.change(nameInput, { target: { value: name } });
    fireEvent.click(submitButton);
}
