import { FunctionComponent } from 'react';
import CreateBookForm from '../../components/CreateBookForm/CreateBookForm';
import './CreateBook.css';

const CreateBook: FunctionComponent = () => {
    return <div id='create__book__content'>

    <CreateBookForm />
    </div>;
}

export default CreateBook;