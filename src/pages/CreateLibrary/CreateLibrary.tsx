import { FunctionComponent } from 'react';
import CreateLibraryForm from '../../components/CreateLibraryForm/CreateLibraryForm';
import './CreateLibrary.css';

const CreateLibrary: FunctionComponent = () => {
    return <div id='create__library__content'>

    <CreateLibraryForm />
    </div>;
}

export default CreateLibrary;