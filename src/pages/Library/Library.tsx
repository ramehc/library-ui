import { FunctionComponent } from "react";
import LibraryDisplay from "../../components/LibraryDisplay/LibraryDisplay";
import "./Library.css";

const Library: FunctionComponent = () => {
    return <div id='library__content'>

    <LibraryDisplay />
    </div>;
}

export default Library;