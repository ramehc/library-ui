import { FunctionComponent } from "react";
import BookDisplay from "../../components/BookDisplay/BookDisplay";
import './Books.css';

const Books: FunctionComponent = () => {

    return <>
    <div id="book__content">
        <BookDisplay/>
    </div>
        
    </>

}

export default Books;