import { faLandmark, faPlusCircle, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FunctionComponent, useState } from "react";
import Library from "../../Interfaces/library";
import { Link, Redirect, useParams } from "react-router-dom";
import { Card, CardBody, CardHeader, Row, Col, Spinner, Alert, Modal, Button, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useMutation, useQuery } from "@apollo/client";
import { DELETE_LIBRARY, LIBRARIES_QUERY } from "../../graphql-queries";
import { toast } from "react-toastify";
import './LibraryDisplay.css';


const LibraryDisplay: FunctionComponent = (_) => {

    const { id } = useParams<{ id: string }>();
    const [isDeleting, setIsDeleting] = useState(false);
    const [selectedLibrary, setSelectedLibrary] = useState<Library>();

    const dismissDeleteModal = () => setSelectedLibrary(undefined);

    const { loading, data, error, refetch } = useQuery(LIBRARIES_QUERY);
    const [deleteLibrary] = useMutation(DELETE_LIBRARY);

    const triggerDelete = (library: Library) => {
        
        setIsDeleting(true);

        deleteLibrary({
            variables: {
                libraryId: library.id
            }
        }).then(() => {
            refetch()
        }).catch(() => {
            toast(`Error trying to delete ${library.branch}`);
            setIsDeleting(false);
        });
    }


    const libraryDisplay = (library: Library) => {
        return <Col sm='6' md='4' key={library.id}>
            <Row>
                &nbsp;
                <h4 className='library__title'>
                {
                    isDeleting && selectedLibrary?.id === library.id ?
                    <Spinner /> :
                    <FontAwesomeIcon icon={faLandmark} />
                }
                &nbsp;
                <Link title="branch" to={`books/${library.id}`}>
                        {library.branch}
                </Link></h4>
                &nbsp;
                {
                    selectedLibrary?.id !== library.id ?
                    <FontAwesomeIcon title='delete__icon' onClick={() => setSelectedLibrary(library)} color='red' size={"xs"} icon={faTimesCircle} />: null
                }
            </Row>


        </Col>
    }



    const isValidId = () => (id.length === 12 || id.length === 24) && !id.includes('book');

    if (id) {
        if (isValidId()) {
            return <Redirect to={`books/${id}`} />;
        }

        return <Redirect to='' />

    }



    if (loading) {
        return <div className="loading__container"><Spinner /></div>
    }

    if (error) {
        return <Alert class="danger">{error.message}</Alert>
    }


    return <>
        <Card>
            <CardHeader><h2>Libraries</h2></CardHeader>
            <CardBody>
                <Row>
                    <Col>
                        <Link to={`/library/create/`}><FontAwesomeIcon icon={faPlusCircle} /> Create Library</Link>
                    </Col>

                </Row>
                <br></br>
                <Row>
                    {
                        data.libraries.map((library: any) => {
                            return libraryDisplay(new Library(library.id, library.branch));
                        })
                    }
                </Row>

            </CardBody>
        </Card>
        {
            selectedLibrary && !isDeleting && <Modal title="delete__modal" isOpen={true} toggle={dismissDeleteModal}>
            <ModalHeader toggle={dismissDeleteModal}>Delete {selectedLibrary?.branch}?</ModalHeader>
            <ModalBody>
                This cannot be undone
        </ModalBody>
            <ModalFooter>
                <Button color="danger" onClick={() => triggerDelete(selectedLibrary)}>Delete</Button>{' '}
                <Button color="secondary" onClick={dismissDeleteModal}>Cancel</Button>
            </ModalFooter>
        </Modal>
        }
    </>;
}

export default LibraryDisplay;