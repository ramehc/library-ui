import React, { FunctionComponent, useState } from "react";
import { Link, Redirect, useParams } from "react-router-dom";
import { Card, CardBody, CardHeader, Row, Col, Spinner, Alert, Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useMutation, useQuery } from "@apollo/client";
import { DELETE_BOOK, LIBRARY_BOOKS_QUERY } from "../../graphql-queries";
import Book from "../../Interfaces/book";
import './BookDisplay.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowCircleLeft, faPlusCircle, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { toast } from "react-toastify";


const BookDisplay: FunctionComponent = (_) => {
    const { libraryId } = useParams<{ libraryId: string }>();

    
    const [isDeleting, setIsDeleting] = useState(false);
    const [selectedBook, setSelectedBook] = useState<Book>();
    const dismissDeleteModal = () => setSelectedBook(undefined);

    const { loading, data, error, refetch } = useQuery(LIBRARY_BOOKS_QUERY, {
        variables: { libraryId },
    });
    const [deleteBook] = useMutation(DELETE_BOOK);

    const triggerDelete = (book: Book) => {
 
        setIsDeleting(true);

        deleteBook({
            variables: {
                libraryId: libraryId,
                bookId: book.id
            }
        }).then(() => {
            refetch();
        }).catch(() => {
            toast(`Error trying to delete ${book.title}`);
            setIsDeleting(false);
        });
    }

    const bookDisplay = (book: Book, index: number) => {
        return <Col sm='12' md='6' lg='4' key={book.id}>
            
            <div title="book__display" className='book'>
                <div className='delete__icon__container'>
                     {
                        selectedBook?.id !== book.id ?
                        <FontAwesomeIcon title="delete__icon" onClick={() => setSelectedBook(book)} color='red' size={"xs"} icon={faTimesCircle} />: null
                    }
                </div>
               
                <div className='cover'>
                    
                        <img alt={book.title} src={`https://source.unsplash.com/random/230x${320 + index}`} />
                        
                   
                    
                </div> 
                <div className='description'>
                    <div>
                        <div className='title'>{book.title}</div>
                        <div className='author'>{book.author.name}</div>
                        <div className='year'>{book.year}</div>
                        {book.genre.length > 0 && <div className='genre'>{book.genre.join(', ')}</div>}

                    </div>

                </div>
            </div>            
        </Col>
    }

    if (!libraryId) {
        return <Redirect to={'/library/'} />;
    }

    if (loading) {
        return <div className="loading__container"><Spinner /></div>;
    }

    if (error) {
        return <Alert color="danger" >{error.message}</Alert>;
    }

    return <>
        <Card>
            <CardHeader><h2> <Link to='/library/'><FontAwesomeIcon icon={faArrowCircleLeft}></FontAwesomeIcon></Link> {data.getLibrary.branch} Library</h2></CardHeader>
            <CardBody>
                <Row>
                    <Col>
                        <Link to={`/library/createbook/${libraryId}`}><FontAwesomeIcon icon={faPlusCircle}/> Create Book</Link>
                    </Col>
                    
                </Row>
                <Row>
                    {
                        data.getLibraryBooks.map((book: Book, index: number) => {
                            return bookDisplay(book, index);
                        })
                    }
                </Row>

            </CardBody>
        </Card>
        { selectedBook !== undefined && !isDeleting &&
            <Modal title="delete__modal" isOpen={true}  toggle={dismissDeleteModal}>
                <ModalHeader toggle={dismissDeleteModal}>Delete {selectedBook?.title}?</ModalHeader>
                <ModalBody>
                    This cannot be undone
            </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={() => triggerDelete(selectedBook)}>Delete</Button>{' '}
                    <Button color="secondary" onClick={dismissDeleteModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        }
    </>
}

export default BookDisplay;