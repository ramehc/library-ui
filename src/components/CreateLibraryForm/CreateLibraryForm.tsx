import { FormEventHandler, FunctionComponent, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label, Card, CardBody, CardHeader, Row, Spinner, Alert } from 'reactstrap';
import { useMutation } from '@apollo/client';
import { ADD_LIBRARY } from '../../graphql-queries';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';


const CreateLibraryForm: FunctionComponent = (props) => {

    const [isSubmitting, setIsSubmitting] = useState(false);
    const [name, setName] = useState<string>();
    const [isCreated, setIsCreated] = useState(false);

    const [addLibrary, {error, loading, client }] = useMutation(ADD_LIBRARY);

    const onFormSubmit: FormEventHandler<HTMLFormElement> = (event) => {
        event.stopPropagation();
        event.preventDefault();

        setIsSubmitting(true);

        addLibrary({
            variables: {
                name: name
            }
        }).then(async() => {
            await client.clearStore();
            setIsCreated(true);
        })
        .catch(() => {
            setIsSubmitting(false);
        })
    }


    if(isCreated){
       return <Redirect to={`/library/`}></Redirect>
    }

    return (
        <Card>
            <CardHeader><h2><Link to={`/library/`}><FontAwesomeIcon icon={faArrowCircleLeft}></FontAwesomeIcon></Link> Create Library</h2></CardHeader>
            <CardBody>
                <Row>
                    {
                        error && <Alert color='danger'>{error.message}</Alert>
                    }
                </Row>

                <Row>
                    <Form  className='form__content' onSubmit={onFormSubmit}>
                        <FormGroup>
                            <Label for="name">Name</Label>
                            <Input disabled={isSubmitting} type='text' name="name" title="name" onChange={(event) => setName(event.target.value)} required={true} placeholder="St. Andrew Parish Library" />
                        </FormGroup>
                        <Button disabled={isSubmitting}>{loading ? <Spinner/> : 'Create'}</Button>
                    </Form>
                </Row>

            </CardBody>
        </Card>
    );
}

export default CreateLibraryForm;