import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Library from './pages/Library/Library';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import Books from './pages/Books/Books';
import CreateBook from './pages/CreateBook/CreateBook';
import CreateLibrary from './pages/CreateLibrary/CreateLibrary';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {

  const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql',
    cache: new InMemoryCache()
  });

  toast.configure();

  return (
    <div id='app'>
      {

console.log(window.location.href)}
      <ApolloProvider client={client}>
        <Router>
          <Switch>
            <Route path="/library/createbook/:libraryId" component={CreateBook}/>
            <Route path="/library/create/" component={CreateLibrary}/>
            <Route path='/library/books/:libraryId' component={Books}/>
            <Route path="/library/:id" component={Library} />
            <Route path="/library/" component={Library} />
            <Redirect from="/" to="/library/"></Redirect>
          </Switch>
        </Router>
      </ApolloProvider>

    </div>

  );
}

export default App;
